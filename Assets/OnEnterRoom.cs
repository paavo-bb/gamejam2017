﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using DG.Tweening;

public class OnEnterRoom : MonoBehaviour {
	public UnityEvent action;
	public RoomNode room;
	public bool waitForSound;
	public float offset;

	void Start () {
		room.onEnter.Add(player => {
			if (action != null) {
				if (waitForSound)
					DOVirtual.DelayedCall(player.lastAudioAt - Time.time - offset, action.Invoke);
				else
					action.Invoke();
			}
			action = null;
		});
	}
}
