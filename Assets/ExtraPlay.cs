﻿using UnityEngine;
using System.Collections;

public class ExtraPlay : MonoBehaviour {
	public AudioClip audio2;
	public FollowPlayer followPlayer;

	static int playCount;

	void Start () {
		if (playCount % 2 == 1) {
			followPlayer.stillAudio.clip = audio2;
		}
		playCount++;
	}
}
