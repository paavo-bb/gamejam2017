﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

public class FollowPlayer : MonoBehaviour {
	public AudioSource stillAudio;
	public AudioSource moveAudio;
	public RoomNode fromRoom, toRoom;

	int currentRoom = -10;

	void Start() {
		var path = findPath(fromRoom, toRoom);
		Debug.Log("FOUND PATH: " + path.Count);
		for (int i = 0; i < path.Count; i++) {
			int j = i;
			path[i].onEnter.Add(player => {
				bool playMove = j == currentRoom && moveAudio != null;
				currentRoom = j + 2;
				if (stillAudio != null && playMove) stillAudio.Stop();
				if (playMove) moveAudio.Play();
				if (j + 1 < path.Count) {
					float duration = player.lastAudioAt - Time.time;
					transform.DOMove(path[j + 2].transform.position, duration);
					if (stillAudio != null && !stillAudio.isPlaying) stillAudio.PlayDelayed(playMove ? moveAudio.clip.length : 0);
				} else
					stillAudio.Stop();
					// else transform.position = new Vector3(100, 100, 100);
			});
		}
	}

	static List<RoomNode> findPath(RoomNode a, RoomNode b) {
		var rooms = new List<RoomNode>(GameObject.FindObjectsOfType<RoomNode>());
		var path = new List<RoomNode>();
		path.Add(a);
		for (int i = 0; i < path.Count; i++) {
			for (int d = 0; d < LevelRandomizer.dirs.Length; d++) {
				var room = PlayerObject.findNode(path[i].transform.position + LevelRandomizer.dirs[d], rooms);
				if (room == null || path.Contains(room)) continue;
				path.Add(room);
				if (room == b) break;
			}
			if (path[path.Count - 1] == b) break;
		}
		if (path[path.Count - 1] != b) return null;

		for (int i = path.Count - 1; i >= 1; i--) {
			if (!path[i].isNeighbour(path[i - 1]))
				path.RemoveAt(i - 1);
		}
		return path;
	}
}
