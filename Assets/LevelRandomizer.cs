﻿using UnityEngine;
using System.Collections.Generic;

public class LevelRandomizer : MonoBehaviour {
	public RoomNode defaultPassage;

	void Awake() {
		var rooms = new List<RoomNode>(GameObject.FindObjectsOfType<RoomNode>());
		var network = buildNetwork(Vector3.zero, rooms);
		int i = 0; while (i < 1000 && !randomize(network, new HashSet<PointInt>())) i++;
		Debug.Log("Cycles: " + i);
		setup(network);
	}

	public static readonly Vector3[] dirs = new Vector3[] { Vector3.left, Vector3.right, Vector3.forward, Vector3.back };

	RoomConnector buildNetwork(Vector3 pos, List<RoomNode> nodes) {
		var room = PlayerObject.findNode(pos, nodes);
		nodes.Remove(room);
		if (room == null) return null;
		var conn = new RoomConnector { room = room };
		for (int i = 0; i < dirs.Length; i++) {
			var c = buildNetwork(pos + dirs[i], nodes);
			if (c != null) {
				if (c.room.isPassage) {
					if (c.passages.Count != 1)
						Debug.LogError("Passage needs two connecting rooms at " + c.room.transform.position);
					conn.passages.Add(new RoomPassage { room = c.room, connectTo = c.passages[0].connectTo });
				} else
					conn.passages.Add(new RoomPassage { room = Instantiate(defaultPassage), connectTo = c });
			}
		}
		return conn;
	}

	bool randomize(RoomConnector conn, HashSet<PointInt> reserved) {
		reserved.Add(conn.position);
		PointInt[] dirs = new PointInt[] { new PointInt(1, 0), new PointInt(0, 1), new PointInt(-1, 0), new PointInt(0, -1) };
		dirs.Shuffle();
		for (int j = 0; j < conn.passages.Count; j++) {
			int i = 0;
			for (; i < dirs.Length; i++) {
				var pos = conn.position.add(dirs[i]).add(dirs[i]);
				if (reserved.Contains(pos)) continue;
				conn.passages[j].position = conn.position.add(dirs[i]);
				conn.passages[j].connectTo.position = pos;
				if (!randomize(conn.passages[j].connectTo, reserved)) {
					//reserved.Remove(pos);
					return false;
				}
				break;
			}
			if (i == dirs.Length) {
				reserved.Remove(conn.position);
				return false;
			}
		}
		return true;
	}

	void setup(RoomConnector conn) {
		conn.room.transform.position = new Vector3(conn.position.x, 0, conn.position.y);
		for (int i = 0; i < conn.passages.Count; i++) {
			conn.passages[i].room.transform.position = new Vector3(conn.passages[i].position.x, 0, conn.passages[i].position.y);
			setup(conn.passages[i].connectTo);
		}
	}
}

class RoomConnector {
	public RoomNode room;
	public List<RoomPassage> passages = new List<RoomPassage>();
	public PointInt position;
}

class RoomPassage {
	public RoomNode room;
	public RoomConnector connectTo;
	public PointInt position;
}

public class RoomPointer {
	public PointInt pos;
	public RoomNode roomPrefab;
}

public struct PointInt {
	public int x, y;

	public PointInt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public PointInt add(PointInt o) {
		return new PointInt(x + o.x, y + o.y);
	}

	public override string ToString() {
		return x + "," + y;
	}
}


static class RandomExtensions
{
	public static void Shuffle<T> (this T[] array)
	{
		int n = array.Length;
		while (n > 1) 
		{
			int k = Random.Range(0, n--);
			T temp = array[n];
			array[n] = array[k];
			array[k] = temp;
		}
	}
}
