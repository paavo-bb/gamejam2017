﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class PlayerObject : MonoBehaviour {
	public AudioSource wallAudio;
	public AudioSource walkAudio;
	public AudioSource[] turningSounds;

	public float lastAudioAt { get; private set; }

	Vector3 mouseStart;

	void Update() {
		if (Input.GetMouseButtonDown(0))
			mouseStart = Input.mousePosition;
		
		if (lastAudioAt > Time.time) return;

		//if (Input.GetKeyDown(KeyCode.LeftArrow)) moveTo(Vector3.left);
		//else if (Input.GetKeyDown(KeyCode.RightArrow)) moveTo(Vector3.right);
		//else if (Input.GetKeyDown(KeyCode.UpArrow)) moveTo(Vector3.forward);
		//else if (Input.GetKeyDown(KeyCode.DownArrow)) moveTo(Vector3.back);

		if (Input.GetKeyDown(KeyCode.A)) turnTo(-transform.right);
		else if (Input.GetKeyDown(KeyCode.D)) turnTo(transform.right);
		else if (Input.GetKeyDown(KeyCode.W)) moveTo(transform.forward);

		else if (Input.GetKeyDown(KeyCode.LeftArrow)) turnTo(-transform.right);
		else if (Input.GetKeyDown(KeyCode.RightArrow)) turnTo(transform.right);
		else if (Input.GetKeyDown(KeyCode.UpArrow)) moveTo(transform.forward);

		if (Input.GetMouseButtonUp(0)) {
			Vector3 mouseTo = Input.mousePosition / Screen.dpi;
			Vector3 mouseFrom = mouseStart / Screen.dpi;
			if (Mathf.Abs(mouseFrom.x - mouseTo.x) < 0.45f) {
				if (mouseTo.y - mouseFrom.y > -0.45f)
					moveTo(transform.forward);
			} else if (Mathf.Abs(mouseTo.x - mouseFrom.x) > Mathf.Abs(mouseTo.y - mouseFrom.y)) {
				if (mouseTo.x < mouseFrom.x)
					turnTo(-transform.right);
				else
					turnTo(transform.right);
			}
		}
	}

	void turnTo(Vector3 dir) {
		//transform.forward = dir;
		playAudio(turningSounds[Mathf.RoundToInt(1 + dir.x + dir.z + Mathf.Abs(dir.z))]);
		DOTween.To(() => transform.forward, v => transform.forward = v, dir, lastAudioAt - Time.time);
	}

	void moveTo(Vector3 dir) {
		var rooms = new List<RoomNode>(GameObject.FindObjectsOfType<RoomNode>());
		Vector3 target = transform.position + dir;
		var currentRoom = findNode(transform.position, rooms);
		var room = findNode(target, rooms);
		if (room != null) {
			
			while (room.isPassage) {
				playAudio(room.bgAudio);
				target += dir;
				room = findNode(target, rooms);
			}

			playAudio(room.walkAudio == null ? walkAudio : room.walkAudio);
			float duration = lastAudioAt - Time.time;
			transform.DOMove(target, duration);
			if (currentRoom.bgAudio != room.bgAudio) {
				if (currentRoom.bgAudio != null) currentRoom.tweenBgAudio(0, duration * 0.5f);
				if (room.bgAudio != null) room.tweenBgAudio(1, duration * 0.5f).SetDelay(duration * 0.5f);
				if (room.enterAudio != null) playAudio(room.enterAudio);
			}
			DOVirtual.DelayedCall(duration * 0.5f, () => room.enter(this));
		} else {
			if (currentRoom.wallAudio == null)
				wallAudio.Play();
			else
				currentRoom.wallAudio.Play();
		}
	}

	public static RoomNode findNode(Vector3 location, List<RoomNode> nodes) {
		for (int i = 0; i < nodes.Count; i++) {
			var dist = Vector3.Distance(nodes[i].transform.position, location);
			if (dist < 0.1f) {
				return nodes[i];
			}
		}
		return null;
	}

	void playAudio(AudioSource audio) {
		if (audio == null) return;
		float time = Mathf.Max(Time.time, lastAudioAt);
		DOVirtual.DelayedCall(time - Time.time, audio.Play);
		lastAudioAt = time + audio.clip.length / audio.pitch;
	}

	public void restart() {
		Application.LoadLevel(Application.loadedLevel);
	}
}
