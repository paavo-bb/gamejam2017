﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

public class RoomNode : MonoBehaviour {
	public AudioSource bgAudio;
	public AudioSource walkAudio;
	public AudioSource wallAudio;
	public AudioSource enterAudio;
	public bool isPassage;
	public List<EnterRoomListener> onEnter = new List<EnterRoomListener>();

	public bool isNeighbour(RoomNode other) {
		float dist = Vector3.Distance(transform.position, other.transform.position);
		return dist < 1.1f && dist > 0.9f;
	}

	public void enter(PlayerObject player) {
		for (int i = 0; i < onEnter.Count; i++)
			onEnter[i].Invoke(player);
	}

	float bgVolume;

	public Tween tweenBgAudio(float volume, float duration) {
		if (bgVolume == 0)
			bgVolume = bgAudio.volume;
		if (volume > 0) {
			bgAudio.volume = 0;
			return bgAudio.DOFade(bgVolume * volume, duration).OnStart(bgAudio.Play);
		} else
			return bgAudio.DOFade(0, duration).OnStart(bgAudio.Stop);
	}
}

public delegate void EnterRoomListener(PlayerObject player);
