﻿using UnityEngine;
using System.Collections;

public class PositionTo : MonoBehaviour {
	public Transform target;
	public Vector3 offset;

	void Update() {
		transform.position = target.transform.position + offset;
	}
}
