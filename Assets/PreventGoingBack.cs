﻿using UnityEngine;
using System.Collections.Generic;

public class PreventGoingBack : MonoBehaviour {

	void Start() {
		GetComponent<RoomNode>().onEnter.Add(onEnter);
	}

	void onEnter(PlayerObject player) {
		Vector3 pos = (player.transform.position - transform.position).normalized + transform.position;
		var room = PlayerObject.findNode(pos, new List<RoomNode>(GameObject.FindObjectsOfType<RoomNode>()));
		if (room != null) room.gameObject.SetActive(false);
		GetComponent<RoomNode>().onEnter.Remove(onEnter);
	}
}
