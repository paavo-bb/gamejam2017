﻿using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {
	public Text text;

	void Start () {
		if (Application.isMobilePlatform) {
			text.text = "Tap to move forward\nSwipe left or right to turn 90°\n\nUse headphones";
		} else {
			text.text = "Press W to move forward\nA or D to turn 90°\n\nUse headphones";
		}
	}
}
